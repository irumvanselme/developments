const express = require('express')
const mongoose= require('mongoose')
var router = express.Router()

const Supplier = mongoose.model('Supplier')
const SupplierCategory = mongoose.model('Supply_Category')

router.post('/',(req,res)=>{
  var supplier = new Supplier()
  supplier.name = req.body.name,
  supplier.company_name = req.body.company_name
  supplier.email = req.body.email
  supplier.telephone = req.body.telephone
  supplier.location = req.body.location
  supplier.save()
   .then(suppliers=>{
        let categories = []
        categories = res.body.categories
        for(i = 0; i < categories.length; i++){
            let supplier_category = new SupplierCategory()
            supplier_category.supplierId = res_.id
            supplier_category.categoryId = categories[i]
            supplier_category.save()
                .then(res => console.log(res))
                .catch(err => s.status(400).send(err))
            }
        res.send(req.body).status(201)
    })
   .catch(err=>res.status(400).send(err))
})
router.put('/',(req,res)=>{
    Supplier.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(suppliers=>res.send(suppliers))
    .catch(err=>res.send(err).status(400))
})
router.get('/',(req,res)=>{
    Supplier.find()
    .then(suppliers=>res.send(suppliers))
    .catch(err=>res.send(err).status(404))
})
router.get('/:id',(req,res)=>{
    Supplier.find({_id:req.params.id})
    .then(suppliers=>res.send(suppliers))
    .catch(err=>res.send(err).status(404))
})
router.get('/byName/:name',(req,res)=>{
    Supplier.find({name:req.params.name})
    .then(suppliers=>res.send(suppliers))
    .catch(err=>res.status(404).send(err))
})
router.delete('/:id',(req,res)=>{
    Supplier.findByIdAndRemove({_id:req.params._id})
    .then(suppliers=>res.send(suppliers))
    .catch(err=>res.status(404).send(err))
})
module.exports=router