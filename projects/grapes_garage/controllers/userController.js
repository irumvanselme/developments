const _ = require('lodash')
const express = require('express')
const hashPassword = require('../utils/hash')
const {User,validate} = require('../model/user.model')

var router = express.Router()
router.get('/',async(req,res)=>{
    const users = await user.find().sort({name:1})
    res.send(users)
})
router.post('/',async(req,res)=>{
    const {error} = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    // let user = await User.find({email:req.body.email}).status(400)
    // if(!user) return res.send('User already registered').status(400)

    user = new User(_.pick(req.body,['name'],['email'],['password']))
    const hashed = await hashPassword(user.password)
    user.password=hashed
    await user.save()
    return res.send(_.pick(user,['id'],['name'],['email'])).status(201)
})
module.exports = router
