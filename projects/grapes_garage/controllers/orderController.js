const express = require('express')
const mongoose= require('mongoose')
var router = express.Router()

const Order = mongoose.model('Order')
const Product = mongoose.model('Product')

router.post('/', async (req,res)=>{
    try {
        var order = new Order({
            clientId: req.body.clientId,
            request_place: req.body.request_place,
            request_destination: req.body.request_destination,
            transporter_brand: req.body.transporter_brand,
            order_date: req.body.order_date,
            status: 0,
            products_id: req.body.products_id,
            produt_quantities: req.body.produt_quantities
          })
          let newOrder = await order.save()
          if(newOrder){
              res.send(newOrder)
          }else{
              res.send("Error in creating the Order")
          }
    } catch (error) {
        res.send(error)
    }
  
   
})
router.put('/',(req,res)=>{
    Order.findOneAndUpdate({_id:req.body._id},req.body,{new:true})
    .then(orders=>res.send(orders))
    .catch(err=>res.send(err).status(400))
})
router.get('/',(req,res)=>{
    Order.find()
    .then(orders=>res.send(orders))
    .catch(err=>res.send(err).status(404))
})
router.get('/finish_order/:id',async (req,res)=>{
    try {
        let order = await Order.findOne({ _id: req.params.id })
        if(order){
            for(i = 0;i < order.products_id.length; i++){
                let newProduct = await Product.findOne({_id: order.products_id[i]})
                if(newProduct){
                    newProduct.items_in_stock -= order.produt_quantities[i]
                    let p = await Product.findOneAndUpdate({ _id: newProduct._id }, newProduct , { new: true })
                }else{
                    res.send("No product found ")
                }
            }
            order.status = 1
            let o = await Order.findOneAndUpdate({_id: order._id},order,{new:true})
            res.send(o)

        }else{
            res.send("No Order Found")
        }
    } catch (error) {
      res.send(error)
    }
})

router.delete('/:id',(req,res)=>{
    Order.findByIdAndRemove({_id:req.params._id})
    .then(orders=>res.send(orders))
    .catch(err=>res.status(404).send(err))
})
module.exports=router