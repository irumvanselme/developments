const mongoose = require('mongoose')
var productSchema = new mongoose.Schema({
    unique_id:{
        type:String,
        required: true
    },
    name:{
        type:String,
        required:true
    },
    categoryId:{
        type:String,
        required: true
    },
    brandId:{
        type:String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    description:{
        type:String,
        required: true
    },
    items_in_stock:{
        type: Number,
        required: true
    },
    imagePaths:{
        type: Array,
        required: true
    }
})
mongoose.model('Product',productSchema)