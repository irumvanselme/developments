<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;

class NoteController extends Controller
{
    public function store(){
        $data = request()->validate([
            'title' => ['required','string','min:5'],
            'description' => ['required','string', 'min:10']
        ]);
        return Note::create($data);
    }
    public function show(){
        return Note::all();
    }
}
