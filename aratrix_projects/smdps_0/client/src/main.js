import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Login from './components/login.vue'
import Home from './components/home.vue'
import NotFound from './components/note-found.vue'
import YesHome from './components/yesHome.vue'
import Jquery from 'jquery'
import axios from 'axios'


Vue.config.productionTip = false

Vue.use(VueRouter)

Vue.prototype.axios = axios;
Vue.prototype.$ = Jquery

const routes = [
  { path: '/login' , component: Login },
  { path: '/' , component: Home },
  { path: '/$/home' , component: YesHome},
  { path: '*', component: NotFound }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
