@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row w-75 m-auto">
        <div class="row col-12 pt-3 pb-3">
            <div class="col-4">
                <img src="/storage/{{ $user->profile->profileImage() }}" alt="my-logo" class='rounded-circle' style='width: 150px;'>
            </div>
            <div class="col-8">
                <div class = 'd-flex justify-content-between align-items-baseline'>
                    <div class = 'd-flex align-items-baseline'>
                        <div class="h4">
                            <h1 class = 'd-inline-block'>{{ $user->username }}</h1>
                        </div>
                    </div>
                    @can('update',$user->profile)
                        <div><a href="/posts/create">Add a new Song</a></div>
                    @endcan
                    
                </div>
                <div class='d-flex'>   
                    <div class='pr-5'><strong>{{ $user->posts->count() }}</strong> Songs </div>
                    <div class='pr-5'><strong>{{ $user->profile->followers->count() }}</strong> Playlists </div>
                </div>
                <hr>
                <div class = 'pt-0'>
                    <div>{{ $user->profile->description }}</div>
                </div>
            </div>
        </div>
        <div class="col-8 p-0">
            <div class="top-menu bg-white p-1 pl-5">
                <h1 class="text-bold">All Songs</h1>
            </div>
            <div class="all-songs m-3">
                <div class="song p-2 row align-items-center">
                    <div class="col-2">
                        <img src="/storage/{{ $user->profile->profileImage() }}" alt="my-logo" class='rounded-circle d-block ml-2' style='width: 50px;'>
                    </div>
                    <div class="col-8">
                        <h4>Paradise by Maher Zain</h4>
                    </div>
                    <div class="col-2">
                        <a href="#">play</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
           <div class="left-box bg-white container p-3 mx-2 py-2 pt-4">
                <div>
                    <img src="/storage/{{ $user->profile->profileImage() }}" alt="my-logo" class='rounded-circle d-block m-auto' style='width: 130px;'>
                </div>
                <div class="px-2 pt-4">
                    <strong>Song Name : </strong><span> Paradise </span><br>
                    <strong>Artist : </strong><span> Maher Zain </span><br>
                    <strong>Album : </strong><span> Best Songs </span><br>
                    <strong>Description : </strong><span> A very Cool Description </span><br>
                </div>
           </div>
        </div>
    </div>
</div>
@endsection
