<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Positions extends Model
{
    protected $fillable = [
        'user_id','title','description','level','class','input_no','school_fees'
    ];
}
