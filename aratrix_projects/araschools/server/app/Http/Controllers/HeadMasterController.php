<?php

namespace App\Http\Controllers;
use App\HeadMasterApplies;
use App\Schools;
use App\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;


use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class HeadMasterController extends Controller
{
    public function apply(Request $request){

        $validator = Validator::make($request->json()->all() , [
            'name' => 'required|string',
            'phone' => 'required|string|unique:head_master_applies',
            'email' => 'required|string|unique:head_master_applies',
            'country' => 'required|string',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 200);
        }

        $headMasterApply = HeadMasterApplies::create([
            'name' => $request->json()->get('name'),
            'phone' => $request->json()->get('phone'),
            'email' => $request->json()->get('email'),
            'country' => $request->json()->get('country'),
            'status' => 0
        ]);

        return response()->json(['data' => $headMasterApply, 'message' => "We will contact by email or phone you in less than 48 hours"]);
    }
    public function accepted(HeadMasterApplies $user){
        $rl = md5($user->id.$user->phone);

        HeadMasterApplies::where('id',$user->id)->update([
            'status'=> 1,
            'randomCode'=> $rl
        ]);

        return response()->json(['message' => 'Successffly Confirmed','link'=>'http://localhost:8000/api/success/'.$rl]);
    }

    public function createSchool(Request $request,$randomCode){
        $user = HeadMasterApplies::where('randomCode',$randomCode)->where('status',1)->first();
        if(isset($user['id'])){

            $validator = Validator::make($request->json()->all() , [
                'name' => 'required|string',
                'email' => 'required|string|unique:head_master_applies',
                'description' => 'required|string',
                'password' => 'required|string',
                'country' => 'required|string',
                'province' => 'required|string',
                'district' => 'required|string',
                'sector' => 'required|string',
            ]);

            if($validator->fails()){
                return response()->json($validator->errors()->toJson(), 200);
            }

            $user1 = User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'level' => 1,
                'password' => Hash::make($request->json()->get('password')),
            ]);

            $school = Schools::create([
                'user_id' => $user1['id'],
                'name' => $request->json()->get('name'),
                'email' => $request->json()->get('email'),
                'description' => $request->json()->get('description'),
                'country' => $request->json()->get('country'),
                'province' => $request->json()->get('province'),
                'district' => $request->json()->get('district'),
                'sector' => $request->json()->get('sector'),
            ]);


            $message = 'Successffly Set as Head Teacher !!!';

            HeadMasterApplies::where('id',$user->id)->update([
                'status'=> 2
            ]);

            return ['school'=>$school,'message'=>$message,'user'=>$user];
        }else{
            return response()->json(['message'=>'Invalid Link']);
        }
    }

    public function getAll(){
        return HeadMasterApplies::all();
    }
}
