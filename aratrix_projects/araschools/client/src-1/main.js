import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Jquery from 'jquery'
import Login from './components/Login.vue'
import Register from './components/Regiter.vue'
import SchoolMaster from './components/SchoolMaster.vue'
import Home from './components/Home.vue'
import Profile from './components/Profile.vue'

Vue.use(VueRouter)
Vue.config.productionTip = false
Vue.prototype.$ = Jquery


const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/login', name: 'Login', component: Login },
  { path: '/register', name: 'Register', component: Register },
  { path: '/school-manager', name: 'SchoolMaster', component: SchoolMaster },
  { path: '/:id/profile', name: 'Profile', component: Profile }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
